<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PeopleFutureTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testListPeoples()
    {
        $response = $this->get('/peoples');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRegisterPeople()
    {
        $data =
            array(
            "first_name" => "Test Riter Prueba",
            "last_name" => "Test Mamani cordova",
            "date_birthday" => "1987-10-04"
        );

        $this
            ->post('peoples/store', $data)
            ->assertStatus(400)
            ->assertRedirect('/peoples')
            ->assertSessionHas('message', 'Se creo correctamente!')
        ;
    }
}
