<?php

namespace Tests\Browser;

use App\People;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Tests\DuskTestCase;

class PeoplesTest extends DuskTestCase
{

    // Configure el archiv .env APP_URL=http://192.168.0.10:8000 => url del servidor

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testPeoples()
    {
        $this->browse(function ($browser) {
            $browser->visit('/peoples')
                ->assertSee('Personas');
        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRegisterPeople()
    {
        $people = [
            'first_name' => 'Test Riter Angel',
            'last_name' => 'Test Mamani cordova',
            'date_birthday' => '1987-10-04',
        ];
        $textExpect = 'Se creo correctamente!';

        $this->browse(function ($browser) use ($people, $textExpect) {
            $browser->visit('/peoples')
                ->type('first_name',$people["first_name"])
                ->type('last_name', $people["last_name"])
                ->type('date_birthday', $people["date_birthday"])
                ->press('Registrar')
                ->assertPathIs('/peoples')
                ->assertSee($textExpect);
        });
    }
}
