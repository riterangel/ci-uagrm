<?php

namespace Tests\Unit;

use App\Http\Controllers\PeopleController;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PeopleUnitTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testValidate()
    {
        $peopleController = new PeopleController();
        $expect = $peopleController->validateForm("Riter Angel", "Mamani Cordova", "1987-02-02");
        $this->assertTrue($expect);
    }
}
