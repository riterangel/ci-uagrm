<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
</head>
<body>

<h2>Personas</h2>

{{ Form::open(array('url'=>'peoples/store')) }}

{{ Form::label('','Nombre:') }}
{{ Form::text('first_name', "",  array('placeholder'=>'')) }}
@if ($errors->has('first_name'))
    <div class="error">{{ $errors->first('first_name') }}</div>
@endif
<br><br>
{{ Form::label('','Apellido:') }}
{{ Form::text('last_name', "",  array('placeholder'=>'')) }}
@if ($errors->has('last_name'))
    <div class="error">{{ $errors->first('last_name') }}</div>
@endif
<br><br>
{{ Form::label('','Fecha de nacimiento(1987-05-06):') }}
{{ Form::text('date_birthday', "",  array('placeholder'=>'')) }}
@if ($errors->has('date_birthday'))
    <div class="error">{{ $errors->first('date_birthday') }}</div>
@endif
<br><br>
{{--{{ Form::submit('Registrar', array('class'=>'btn btn-success'))}}--}}
<button type="submit" class="btn btn-primary btn-sm">Registrar</button>

{{ Form::close() }}
<br><br><br><br>
@if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
@endif
<br><br>
<table id="tableusers" class="table table-striped table-condensed">
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>Fecha Nacimiento</th>
        <th></th>
    </tr>
    </thead>
    @foreach ($peoples as $people)
        <tr>
            <td>{{ $people->first_name }}</td>
            <td>{{ $people->last_name }}</td>
            <td>{{ $people->date_birthday }}</td>
            <td class="col-btn">
                <a href="{{ route('peoples.delete', $people->id) }}" onclick="return confirm('¿ Está seguro que desea Eliminar ?');">
                   Eliminar
                </a>
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>