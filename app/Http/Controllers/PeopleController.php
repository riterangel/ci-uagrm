<?php

namespace App\Http\Controllers;

use App\People;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use View;

class PeopleController extends Controller
{

    private $rules = [
        'first_name' => "required|string",
        'last_name' => 'required|string',
        'date_birthday' => 'required|date'
    ];


    public function index()
    {
        $peoples = People::all();
        return View::make('peoples')
            ->with('peoples',$peoples);
    }


    public function store(Request $request)
    {
        $isValid = $this->validateForm($request->get("first_name"), $request->get("last_name"), $request->get("date_birthday"));

        if ($isValid) {
            try {
                People::create($request->all());

                $request->session()->flash('message', 'Se creo correctamente!');
                return redirect('peoples');
            } catch (\Exception $exception) {

            }
        }

        return redirect('peoples')
            ->withErrors($request->validate($this->rules))
            ->withInput();
    }

    public function delete($id)
    {
        People::destroy($id);
        return redirect('peoples');
    }

    public function validateForm($firstName = null, $lastName = null, $birthday = null) {
        return !empty($firstName) && !empty($lastName) && !empty($birthday);
    }

}
