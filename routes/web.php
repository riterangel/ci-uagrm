<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('peoples', array(
    'as' => 'peoples',
    'uses' => 'PeopleController@index'));

Route::post('peoples/store', 'PeopleController@store');

Route::get('peoples/{id}', array(
    'as' => 'peoples.delete',
    'uses' => 'PeopleController@delete'));
